package ru.tinkoff.service

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.util.Try

object RestService extends App {

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  val route: Route = ???

  val port = Try(system.settings.config.getInt("akka.http.server.port")).getOrElse(8080)
  print(port)
  val bindingFuture = Http().bindAndHandle(route, "localhost", port)

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
